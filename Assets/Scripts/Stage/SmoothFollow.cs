using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
	// 따라다닐 타겟
	[SerializeField]
	private Transform target;

	// x-z 평면에서 타겟까지의 거리
	[SerializeField]
	private float distance = 10.0f;
	// 카메라의 높이값
	[SerializeField]
	private float height = 5.0f;

	[SerializeField]
	private float rotationDamping;
	[SerializeField]
	private float heightDamping;

	void LateUpdate()
	{
		// 타겟이 없으면 종료
		if (!target)
			return;

		// 현재 회전 각도 계산
		var wantedRotationAngle = target.eulerAngles.y;
		var wantedHeight = target.position.y + height;
		var currentRotationAngle = transform.eulerAngles.y;
		var currentHeight = transform.position.y;

		// y-축을 기준으로 회전각도 계산
		currentRotationAngle = Mathf.LerpAngle(
			currentRotationAngle,
			wantedRotationAngle,
			rotationDamping * Time.deltaTime);

		// 이동 높이 계산
		currentHeight = Mathf.Lerp(
			currentHeight,
			wantedHeight,
			heightDamping * Time.deltaTime);

		// 오일러각을 쿼터니언으로 변환
		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

		// x-z 평면상에서 카메라의 위치를
		// 타겟의 뒷쪽에서 일정거리 떨어진 위치로 설정
		transform.position = target.position;
		transform.position -= currentRotation * Vector3.forward * distance;

		// 카메라 높이 설정
		transform.position = new Vector3(transform.position.x, 
			currentHeight, transform.position.z);

		// 항상 타겟을 바라보기
		transform.LookAt(target);
	}
}
