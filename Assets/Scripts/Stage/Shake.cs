using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
    public Transform shakeCamera; // 셰이크 효과를 줄 카메라의 Transform을 저장할 변수
    public bool shakeRotate = false; // 회전시킬 것이지를 판단 할 변수
    private Vector3 originPos; // 초기 좌표와 회전값을 저장할 변수
    private Quaternion originRot;

    void Start()
    {
        // 카메라의 초깃값을 저장
        originPos = shakeCamera.localPosition;
        originRot = shakeCamera.localRotation;
    }

    public IEnumerator ShakeCamera(float duration = 0.05f
                                    , float magnitudePos = 0.03f
                                    , float magnitudeRot = 0.1f)
    {
        float passTime = 0.0f; // 지나간 시간을 누적할 변수

        while (passTime < duration)
        {
            Vector3 shakePos = Random.insideUnitSphere;
            shakeCamera.localPosition = shakePos * magnitudePos;

            if (shakeRotate)
            {
                Vector3 shakeRot = new Vector3(0, 0, Mathf.PerlinNoise(Time.time * magnitudeRot, 0.0f));
                shakeCamera.localRotation = Quaternion.Euler(shakeRot);
            }

            passTime += Time.deltaTime;

            yield return null;
        }
    }
}
