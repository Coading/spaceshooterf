using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAnim
{
    public AnimationClip idle;
    public AnimationClip runF;
    public AnimationClip runB;
    public AnimationClip runL;
    public AnimationClip runR;
}

public class PlayerCtrl : MonoBehaviour
{
    private float h = 0.0f;
    private float v = 0.0f;
    private float r = 0.0f;

    // 점근해야 하는 컴포넌트는 반드시 변수에 할당한 후 사용    
    private Transform tr;
    // 이동 속도 변수
    public float moveSpeed = 10.0f;
    // 회전 속도 변수
    public float rotSpeed = 80.0f;

    public PlayerAnim playerAnim; // 인스펙터 뷰에 표시할 애니메이션 클래스 변수

    [HideInInspector]
    public Animation anim; // Animation 컴포넌트를 저장하기 위한 변수

    private void OnEnable()
    {
        GameManager.OnItemChange += UpdateSetup;
    }

    void UpdateSetup()
    {
        moveSpeed = GameManager.instance.gameData.speed;
    }

    void Start()
    {
        // 스크립트가 실행된 후 처음 수행되는 Start 함수에서
        // Transform 컴포넌트를 할당
        tr = this.GetComponent<Transform>(); // 컴포넌트 캐싱

        anim = this.GetComponent<Animation>(); // Animation 컴포넌트 캐싱
        anim.clip = playerAnim.idle;
        anim.Play();

        moveSpeed = GameManager.instance.gameData.speed;
    }

    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        r = Input.GetAxis("Mouse X");

        // 전후좌우 이동 방향 벡터 계산
        Vector3 moveDir = (Vector3.forward * v) + (Vector3.right * h);

        // Translate (이동 방향 * 속도 * 변위값 * Time.deltaTime, 기준좌표)
        tr.Translate(moveDir.normalized * moveSpeed * Time.deltaTime, Space.Self);

        // Vector3.up 축을 기준으로 rotSpeed만큼의 속도록 회전        
        tr.Rotate(Vector3.up * Time.deltaTime * rotSpeed * r, Space.Self);

        if (v >= 0.1f)
        {
            anim.CrossFade(playerAnim.runF.name, 0.3f); // 전진 애니메이션
        }
        else if (v <= -0.1)
        {
            anim.CrossFade(playerAnim.runB.name, 0.3f); // 후진 애니메이션
        }
        else if (h >= 0.1f)
        {
            anim.CrossFade(playerAnim.runR.name, 0.3f); // 오른쪽 이동 애니메이션
        }
        else if (h <= -0.1f)
        {
            anim.CrossFade(playerAnim.runL.name, 0.3f); // 왼쪽 이동 애니메이션
        }
        else
        {
            anim.CrossFade(playerAnim.idle.name, 0.3f); // 입력이 없을때 대기동작 애니메이션
        }
    }
}
