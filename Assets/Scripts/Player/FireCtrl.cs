using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// 총알 발사와 재장전 오디오 클립을 저장할 구조체
/// </summary>
[System.Serializable]
public struct PlayerSfx
{
    public AudioClip[] fire;
    public AudioClip[] reload;
}

public class FireCtrl : MonoBehaviour
{
    // 무기 타입
    public enum WeaponType
    {
        RIFLE = 0, // 라이플
        SHOTGUN // 샷건
    }

    // 주인공이 현재 들고 있는 무기를 저정할 변수
    public WeaponType currWeapon = WeaponType.RIFLE;


    // 총알 프리팹
    public GameObject bullet;
    // 총알 발사 좌표
    public Transform firePos;
    // 탄피 추출 파티클
    public ParticleSystem cartridge;
    // 총구 화염 파티클
    private ParticleSystem muzzleFlash;

    // AudioSource 컴포넌트를 저정할 변수
    private AudioSource _audio;
    // 오디오 클립을 저장할 변수
    public PlayerSfx playerSfx;
    // Shake 클래스를 저장할 변수
    private Shake shake;

    public Image magazineImg;
    public Text magazineText;
    public int maxBullet = 10;
    public int remainingBullet = 10;
    public float reloadTime = 2.0f;
    private bool isReloading = false;

    public Sprite[] weaponIcons; // 변경할 무기 이미지
    public Image weaponImage; // 교채할 무기 이미지 UI

    private int enemyLayer; // 적 캐릭터의 레이어 값을 저장할 변수
    private int obstacleLayer; // 장애물의 레이어 값을 저장할 변수
    private int layerMask; // 레이어 마스크의 비트 연산을 위한 변수
    private bool isFire = false; // 자동 발사 여부를 판단할 변수
    private float nextFire; // 다음 발사 시간을 저장할 변수
    public float fireRate = 0.1f; // 총알의 발사 간격

    private void Start()
    {
        magazineImg.fillAmount = 1.0f;
        UpdateBulletText();

        // FirePos 하위에 있는 컴포넌트 추출
        muzzleFlash = firePos.GetComponentInChildren<ParticleSystem>();
        // 오디오 클립을 저장할 변수
        _audio = GetComponent<AudioSource>();
        // Shake 스크립트 추출
        shake = GameObject.Find("CameraRig").GetComponent<Shake>();
        //적 캐릭터의 레이어 값을 추출
        enemyLayer = LayerMask.NameToLayer("ENEMY");
        // 장애물의 레이어 값을 추출
        obstacleLayer = LayerMask.NameToLayer("OBSTACLE");
        // 레이어 마스크의 비트 연산 (OR 연산)
        layerMask = 1 << obstacleLayer | 1 << enemyLayer;
    }

    private void Update()
    {
        Debug.DrawRay(firePos.position, firePos.forward * 20.0f, Color.green);

        if (EventSystem.current.IsPointerOverGameObject()) return;

        /*
         * Racast (발사 원점, 발사 방향, out 반환 받을 변수, 도달 거리, 검출할 레이어)
         */ 
        RaycastHit hit; // 레이캐스트에 검출된 객체의 정보를 저장할 변수
        /*
         * RaycastHit 구조체의 주요 속성..
         * collider : 맞은 게임오브젝트의 Collider 반환
         * transform : 맞은 게임오브젝트의 Transform 반환 
         * point : 맞은 위치의 월드 좌표값 반환
         * distance : 발사 위치와 맞은 위치 사이의 거리
         * normal : Ray가 맞은 표면의 법선 벡터
         */
        if (Physics.Raycast(firePos.position    // 광선의 발사 원점 좌표
                            , firePos.forward   // 관선의 발사 방향 벡터
                            , out hit           // 검출된 객체의 정보를 반환 받을 변수     
                            , 20.0f             // 광선의 도달 거리
                            , layerMask)) // 검출할 레이어
        {
            isFire = (hit.collider.CompareTag("ENEMY"));
        }
        else
        {
            isFire = false;
        }

        //레이캐스트에 적 캐릭터가 닿았을 때 자동 발사
        if (!isReloading && isFire)
        {
            if (Time.time > nextFire)
            {
                // 총알 수를 하나식 감소 시킨다
                --remainingBullet;
                Fire();

                // 남은 촐알이 없을 경우 재장전 코루틴 호출
                if (remainingBullet == 0)
                {
                    StartCoroutine(Reloading());
                }

                // 다음 총알 발사 시간을 계산
                nextFire = Time.time + fireRate;
            }
        }


        // 마우스 왼쪽 버튼을 클릭했을 때 Fire 함수 호출
        if (!isReloading && Input.GetMouseButtonDown(0))
        {
            --remainingBullet;
            Fire();

            if (remainingBullet == 0)
            {
                StartCoroutine(Reloading());
            }
        }
    }

    void Fire()
    {
        // 셰이크 효과 호출
        StartCoroutine(shake.ShakeCamera());
        // Bullet 프리팹을 동적으로 생성
        //Instantiate(bullet, firePos.position, firePos.rotation);
        var _bullet = GameManager.instance.GetBullet();
        if (_bullet != null)
        {
            _bullet.transform.position = firePos.position;
            _bullet.transform.rotation = firePos.rotation;
            _bullet.SetActive(true);
        }

        //파티클 실행
        cartridge.Play();
        //총구 화염 파티클 실행
        muzzleFlash.Play();
        // 사운드 발생
        FireSfx();

        magazineImg.fillAmount = (float)remainingBullet / (float)maxBullet;
        UpdateBulletText();
    }

    void FireSfx()
    {
        // 현재 들고 있는 무기의 오디오 클립을 가져옴
        var _sfx = playerSfx.fire[(int)currWeapon];
        // 사운드 발생
        _audio.PlayOneShot(_sfx, 1.0f);
    }

    IEnumerator Reloading()
    {
        isReloading = true;
        _audio.PlayOneShot(playerSfx.reload[(int)currWeapon], 1.0f);
        yield return new WaitForSeconds(playerSfx.reload[(int)currWeapon].length + 0.3f);

        isReloading = false;
        magazineImg.fillAmount = 1.0f;
        remainingBullet = maxBullet;
        UpdateBulletText();
    }

    void UpdateBulletText()
    {
        magazineText.text = string.Format("<color=#ff0000>{0}</color>/{1}", remainingBullet, maxBullet);
    }

    public void OnChangeWeapon()
    {
        currWeapon = (WeaponType)((int)++currWeapon % 2);
        weaponImage.sprite = weaponIcons[(int)currWeapon];
    }
}
