using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damage : MonoBehaviour
{
    private const string bulletTag = "BULLET";
    private const string enemyTag = "ENEMY";

    private float initHp = 100.0f;
    public float currHp;
    public Image bloodScreen; // BloodScreen 텍스처를 저장하기 위한 변수 
    public Image hpBar;
    private readonly Color initColor = new Vector4(0, 1.0f, 0.0f, 1.0f);
    private Color currcolor;

    // 델리게이트 및 이벤트 선언
    public delegate void PlayerDieHandler();
    public static event PlayerDieHandler OnPlayerDie;

    private void OnEnable()
    {
        GameManager.OnItemChange += UpdateSetup;
    }

    void UpdateSetup()
    {
        initHp = GameManager.instance.gameData.hp;
        currHp += GameManager.instance.gameData.hp - currHp;
    }

    void Start()
    {
        initHp = GameManager.instance.gameData.hp;

        currHp = initHp;

        hpBar.color = initColor;
        currcolor = initColor;
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == bulletTag)
        {
            Destroy(coll.gameObject);

            StartCoroutine(ShowBloodScreen());

            currHp -= 5.0f;
            Debug.Log("Player HP = " + currHp.ToString());

            // 생명 게이지의 색상 및 크기 변경 함수를 호출
            DisplayHpbar ();

            if (currHp <= 0.0f)
            {
                PlayerDie();
            }
        }
    }

    IEnumerator ShowBloodScreen()
    {
        // BloodScreen 텍스처의 알파값을 불규칙하게 변경
        bloodScreen.color = new Color(1, 0, 0, Random.Range(0.2f, 0.3f));
        yield return new WaitForSeconds(0.1f);
        // BloodScreen 텍스처의 색상을 모두 0으로 변경
        bloodScreen.color = Color.clear;
    }

    void PlayerDie()
    {
        OnPlayerDie();
        GameManager.instance.isGameOver = true;

        //Debug.Log("PlayerDie !");

        //GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        //for (int i = 0; i < enemies.Length; i++)
        //{
        //    enemies[i].SendMessage("OnPlayerDie", SendMessageOptions.DontRequireReceiver);
        //}
    }

    void DisplayHpbar()
    {
        if ((currHp / initHp) > 0.5f)
            currcolor.r = (1 - (currHp / initHp)) * 2.0f;
        else
            currcolor.g = (currHp / initHp) * 2.0f;

        hpBar.color = currcolor;
        hpBar.fillAmount = (currHp / initHp);
    }
}
