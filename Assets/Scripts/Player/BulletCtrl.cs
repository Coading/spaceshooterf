using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrl : MonoBehaviour
{
    // �Ѿ��� �ı���
    public float damage = 20.0f;
    // �Ѿ� �߻� �ӵ�
    public float speed = 1000.0f;

    private Transform tr;
    private Rigidbody rb;
    private TrailRenderer trail;

    private void Awake()
    {
        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        trail = GetComponent<TrailRenderer>();

        damage = GameManager.instance.gameData.damage;
    }

    private void OnEnable()
    {
        rb.AddForce(this.transform.forward * speed);
        GameManager.OnItemChange += UpdateSetup;
    }

    void UpdateSetup()
    {
        damage = GameManager.instance.gameData.damage;
    }

    private void OnDisable()
    {
        trail.Clear();
        tr.position = Vector3.zero;
        tr.rotation = Quaternion.identity;
        rb.Sleep();
    }

    //void Start()
    //{
    //GetComponent<Rigidbody>().AddForce(this.transform.forward * speed);
    //}
}
