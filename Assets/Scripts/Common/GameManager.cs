using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DataInfo;

public class GameManager : MonoBehaviour
{
    [Header("Enemy Create Info")]
    public Transform[] points;
    
    public GameObject enemy;
    public float createTime = 2.0f;
    public int maxEnemy = 3;
    public bool isGameOver = false;    

    public static GameManager instance = null;

    [Header("Object Pool")]
    public GameObject bulletPrefab;
    public int maxPool = 10;
    public List<GameObject> bulletPool = new List<GameObject>();

    private bool isPaused;

    public CanvasGroup inventoryCG;

    //[HideInInspector] public int killCount;
    [Header("GameData")]
    public Text killCountTxt;
    private DataManager dataManager;
    //public GameData gameData;
    public GameDataObject gameData; // ScriptableObject 를 연결할 변수
    public delegate void ItremChangeDelegate();
    public static event ItremChangeDelegate OnItemChange;
    //SlotList 게임 오브젝트를 저장할 변수
    private GameObject slotList;
    //ItemList 하위에 있는 네 개의 아이템을 저장할 배열s
    public GameObject[] itemObjects;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        dataManager = GetComponent<DataManager>();
        dataManager.Initialize();

        // 인벤토리에 추가된 아이템을 검색하기 위해 SlotList 게임오브젝트 추출
        slotList = inventoryCG.transform.Find("SlotList").gameObject;

        LoadGameDate();

        CreatePooling();
    }

    void LoadGameDate()
    {
        //GameData data = dataManager.Load();
        //gameData.hp = data.hp;
        //gameData.damage = data.damage;
        //gameData.speed = data.speed;
        //gameData.killCount = data.killCount;
        //gameData.equipItem = data.equipItem;

        if (gameData.equipItem.Count > 0) {
            InventorySetup();
        }

        //killCount = PlayerPrefs.GetInt("KILL_COUNT", 0);
        killCountTxt.text = "KILL " + gameData.killCount.ToString("0000");
    }

    void SaveGameData()
    {
        //dataManager.Save(gameData);
        UnityEditor.EditorUtility.SetDirty(gameObject);
    }

    void InventorySetup()
    {
        var slots = slotList.GetComponentsInChildren<Transform>();
        for (int i = 0; i < gameData.equipItem.Count; i++)
        {
            for (int j = 1; j < slots.Length; j++)
            {
                if (slots[j].childCount > 0) continue;

                int itemIndex = (int)gameData.equipItem[i].itemType;

                itemObjects[itemIndex].GetComponent<Transform>().SetParent(slots[j]);
                itemObjects[itemIndex].GetComponent<ItemInfo>().itemData = gameData.equipItem[i];
                
                break;
            }
        }
    }

    // 인벤토리에 아이템을 추가했을 때 데이터의 정보를 갱신하는 함수
    public void AddItem(Item item)
    {
        // 보유 아이템에 같은 아이템이 있으면 추가하지 않고 빠져나감
        if (gameData.equipItem.Contains(item)) return;

        // 아이템을 GameData.equipItem 배열에 추가
        gameData.equipItem.Add(item);

        // 아이템의 종류에 따라 분기 처리
        switch (item.itemType)
        {
            case Item.ItemType.HP:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.hp += item.value;
                else
                    gameData.hp += gameData.hp * item.value;
                break;

            case Item.ItemType.DAMAGE:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.damage += item.value;
                else
                    gameData.damage += gameData.damage * item.value;
                break;

            case Item.ItemType.SPEED:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.speed += item.value;
                else
                    gameData.speed += gameData.speed * item.value;
                break;

            case Item.ItemType.GRENADE:
                break;
        }

        //.asset 파일에 데이터 저장
        UnityEditor.EditorUtility.SetDirty(gameObject);
        // 아이템이 변경된 것을 실시간으로 반여하기 위해 이벤트를 발생시킴
        OnItemChange();
    }

    // 인벤토리에서 아이템을 제거했을 때 데이터를 갱신하는 함수
    public void RemoveItem(Item item)
    {
        // 아이템을 GameData.equipItem 배열에서 삭제
        gameData.equipItem.Remove(item);

        switch (item.itemType)
        {
            case Item.ItemType.HP:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.hp -= item.value;
                else
                    gameData.hp = gameData.hp / (1.0f + item.value);
                break;

            case Item.ItemType.DAMAGE:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.damage -= item.value;
                else
                    gameData.damage = gameData.damage / (1.0f + item.value);
                break;

            case Item.ItemType.SPEED:
                if (item.itemCalc == Item.ItemCalc.INC_VALUE)
                    gameData.speed -= item.value;
                else
                    gameData.speed = gameData.speed / (1.0f + item.value);
                break;

            case Item.ItemType.GRENADE:
                break;
        }

        UnityEditor.EditorUtility.SetDirty(gameObject);

        OnItemChange();
    }

    private void Start()
    {
        OnInventoryOpen(false);
        points = GameObject.Find("SpawnPointGroup").GetComponentsInChildren<Transform>();
        if (points.Length > 0) {
            StartCoroutine(this.CreateEnemy());
        }
    }

    IEnumerator CreateEnemy()
    {
        while (!isGameOver)
        {
            int enemyCount = (int)GameObject.FindGameObjectsWithTag("ENEMY").Length;
            if (enemyCount < maxEnemy)
            {
                yield return new WaitForSeconds(createTime);

                int idx = Random.Range(1, points.Length);
                Instantiate(enemy, points[idx].position, points[idx].rotation);
            }
            else
            {
                yield return null;
            }
        }
    }

    public GameObject GetBullet()
    {
        for (int i = 0; i < bulletPool.Count; i++)
        {
            if (bulletPool[i].activeSelf == false)
            {
                return bulletPool[i];
            }
        }

        return null;
    }

    public void CreatePooling()
    {
        GameObject objectPools = new GameObject("ObjectPools");
        for (int i = 0; i < maxPool; i++)
        {
            var obj = Instantiate<GameObject>(bulletPrefab, objectPools.transform);
            obj.name = "Bullet_" + i.ToString("00");
            obj.SetActive(false);
            bulletPool.Add(obj);
        }
    }

    public void OnPauseClick()
    {
        isPaused = !isPaused; // 일시 정지 값을 토글시킴
        Time.timeScale = (isPaused) ? 0.0f : 1.0f; // Time Scale 이 0이면 정지, 1이면 정상 속도

        var playerObj = GameObject.FindGameObjectWithTag("PLAYER");
        var scripts = playerObj.GetComponents<MonoBehaviour>();

        foreach (var script in scripts)
        {
            script.enabled = !isPaused;
        }

        var canvasGroup = GameObject.Find("Panel - Weapon").GetComponent<CanvasGroup>();
        canvasGroup.blocksRaycasts = !isPaused;
    }

    public void OnInventoryOpen(bool isOpened)
    {
        inventoryCG.alpha = (isOpened) ? 1.0f : 0.0f;
        inventoryCG.interactable = isOpened;
        inventoryCG.blocksRaycasts = isOpened;
    }

    public void IncKillCount()
    {
        ++gameData.killCount;
        killCountTxt.text = "KILL " + gameData.killCount.ToString("0000");
        //PlayerPrefs.SetInt("KILL_COUNT", killCount);
    }

    private void OnApplicationQuit()
    {
        SaveGameData();
    }    
}
