using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DataInfo;

[CreateAssetMenu(fileName = "GameDataSO", menuName = "Create GameData", order = 1/*생성할 메뉴의 순서를 결정한다*/)]
public class GameDataObject : ScriptableObject
{
    public int killCount = 0; // 사망한 적 캐릭터의 수
    public float hp = 120.0f; // 주인공의 초기 설정
    public float damage = 25.0f; // 총알의 데미지
    public float speed = 6.0f; // 이동 속도
    public List<Item> equipItem = new List<Item>(); // 취득한 아이템
}
