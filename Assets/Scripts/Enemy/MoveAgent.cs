using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveAgent : MonoBehaviour
{
    public List<Transform> wayPoints;
    public int nextIdx;
    private NavMeshAgent agent;

    private readonly float patrolSpeed = 1.5f; // 순찰할때 이동속도
    private readonly float traceSpeed = 4.0f; // 추적할때 이동속도
    private float damping = 1.0f; // 회전할 때의 속도를 조절하는 계수
    private Transform enemyTr; // 적 캐릭터의 Transform 컴포넌트를 저장할 변수

    // 순찰 여부를 판단하는 변수
    private bool _patrolling;
    // patrolling 프로퍼티 정의 (getter, setter)
    public bool patrolling
    {
        get { return _patrolling;  }
        set
        {
            _patrolling = value;
            if (_patrolling)
            {
                agent.speed = patrolSpeed;
                damping = 1.0f; // 순찰 상태의 회전 계수
                MoveWayPoint();
            }
        }
    }

    // 추적 대상의 위치를 저장하는 변수
    private Vector3 _traceTarget;
    // traceTarget 프로퍼티 정의 (getter, setter)
    public Vector3 traceTarget
    {
        get { return _traceTarget;  }
        set
        {
            _traceTarget = value;
            agent.speed = traceSpeed;
            damping = 7.0f; // 추적 상태의 회전계수

            TraceTarget(_traceTarget);
        }
    }

    // NaveMeshAgent의 이동 속도에 대한 프로퍼티 정의 (getter)
    public float speed
    {
        get { return agent.velocity.magnitude;  }
    }

    void Start()
    {
        //적 캐릭터의 Transform 컴포너트 추출 후 변수에 저장
        enemyTr = GetComponent<Transform>();

        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        agent.updateRotation = false; // 자동으로 회전하는 기능을 비활성화
        agent.speed = patrolSpeed;

        var group = GameObject.Find("WayPointGroup");
        if (group != null)
        {
            group.GetComponentsInChildren<Transform>(wayPoints);
            wayPoints.RemoveAt(0);

            // 첫 번째로 이동할 위치를 불규칙하게 추출
            nextIdx = Random.Range(0, wayPoints.Count);
        }

        //MoveWayPoint();
        this.patrolling = true;
    }

    void MoveWayPoint ()
    {
        if (agent.isPathStale) return;

        agent.destination = wayPoints[nextIdx].position;
        agent.isStopped = false;
    }

    // 주인공을 추적할 때 이동시키는 함수
    void TraceTarget(Vector3 pos)
    {
        if (agent.isPathStale) return;

        agent.destination = pos;
        agent.isStopped = false;
    }

    public void Stop ()
    {
        agent.isStopped = true;
        // 바로 정지하기 위해 속도를 0으로 설정
        agent.velocity = Vector3.zero;
        _patrolling = false;
    }

    void Update()
    {
        // 적 캐릭터가 이동 중일 때만 회전
        if (agent.isStopped == false)
        {
            Quaternion rot = Quaternion.LookRotation(agent.desiredVelocity);
            enemyTr.rotation = Quaternion.Slerp(enemyTr.rotation, rot, Time.deltaTime * damping);
        }

        // 순찰 모드가 아닐 경우 이후 로직을 수행하지 않음
        if (!_patrolling) return;

        if (agent.velocity.sqrMagnitude >= 0.15f * 0.15f &&
            agent.remainingDistance <= 0.5f)
        {
            //nextIdx = ++nextIdx % wayPoints.Count;
            nextIdx = Random.Range(0, wayPoints.Count);
            MoveWayPoint();
        }
    }
}
